
#ifndef BASE_TOOLS_GRAPHICS_SAVE_H_
#define BASE_TOOLS_GRAPHICS_SAVE_H_

#include <interface/file.h>
#include <graphics/color.h>
#include <graphics/texture2d.h>

namespace Graphics {

bool save(Interface::File::Shared file, Texture2D<RGB8>& texture);
bool save(Interface::File::Shared file, Texture2D<RGBA8>& texture);

// sRGB
bool save(Interface::File::Shared file, Texture2D<SRGB8>& texture);

} // ns Graphics

#endif // BASE_TOOLS_GRAPHICS_SAVE_H_
