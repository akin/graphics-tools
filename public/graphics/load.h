
#ifndef BASE_TOOLS_GRAPHICS_LOAD_H_
#define BASE_TOOLS_GRAPHICS_LOAD_H_

#include <interface/file.h>
#include <graphics/color.h>
#include <graphics/texture2d.h>

namespace Graphics {

bool load(Interface::File::Shared file, Texture2D<RGB8>& texture);
bool load(Interface::File::Shared file, Texture2D<RGBA8>& texture);

} // ns Base

#endif // BASE_TOOLS_GRAPHICS_LOAD_H_
