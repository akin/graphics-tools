
#include <graphics/save.h>

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

#include <fstream>

#include <string>
#include <graphics/color.h>
#include <graphics/texture2d.h>

namespace Graphics {

void imageWrittandoFunc(void *context, void *data, int size)
{
    Interface::File *file = (Interface::File*)context;
    file->write((size_t)size, (uint8_t*)data);
}

bool save(Interface::File::Shared file, Texture2D<RGB8>& texture)
{
    int w = (int)texture.getWidth();
    int h = (int)texture.getHeight();

    int success = stbi_write_png_to_func(
        imageWrittandoFunc,
        &(*file),
        w,h,3, texture.data(),0);
    file->flush();

    return success != 0;
}

bool save(Interface::File::Shared file, Texture2D<RGBA8>& texture)
{
    int w = (int)texture.getWidth();
    int h = (int)texture.getHeight();
    
    int success = stbi_write_png_to_func(
        imageWrittandoFunc,
        &(*file),
        w,h,4, texture.data(),0);
    file->flush();

    return success != 0;
}

// sRGB
bool save(Interface::File::Shared file, Texture2D<SRGB8>& texture)
{
    int w = (int)texture.getWidth();
    int h = (int)texture.getHeight();

    int success = stbi_write_png_to_func(
        imageWrittandoFunc,
        &(*file),
        w,h,3, texture.data(),0);
    file->flush();

    return success != 0;
}

} // ns Graphics

